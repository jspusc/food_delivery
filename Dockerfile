FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUBUFFERED 1

WORKDIR /food_delivery
RUN python -m pip install --upgrade pip
COPY . .
RUN pip install -r requirements.txt
