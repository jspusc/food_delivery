from django.contrib.auth.views import LoginView
from users.forms import UserLoginForm, UserRegisterForm
from users.models import User
from django.views.generic import CreateView, View
from django.urls import reverse_lazy
from django.shortcuts import redirect, render
from django.core.exceptions import ValidationError

from django.contrib import messages
from django.utils.html import strip_tags

from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from users.tokens import account_activation_token

from django.utils.encoding import force_text
from django.contrib.auth import login, logout


class UserLoginView(LoginView):
    authentication_form = UserLoginForm
    template_name = 'user/login.html'

    def get(self, request, *args, **kwargs):
        # prevent routing to login if already logged in
        if request.user.is_authenticated:
            return redirect('home')
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.user_cache
        if not user.is_active:
            messages.warning(self.request, f'{user.username}, Vaš račun nije aktiviran. '
                                           f'Slijedite upute za aktivaciju koje ste primili '
                                           f'prilikom stvaranja korisničkog računa na e-adresi {user.email}.')
            return render(self.request, self.template_name, {'form': form})
        login(self.request, user)
        return redirect('home')


class UserRegisterView(CreateView):
    model = User
    form_class = UserRegisterForm
    template_name = 'user/register.html'
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        # prevent routing to register if already logged in
        if request.user.is_authenticated:
            return redirect('home')
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = f'Aktivirajte svoj {current_site} korisnički račun'
            html_message = render_to_string(
                'user/account_activation_email_message.html',
                {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }
            )
            user.email_user(subject, strip_tags(html_message), html_message=html_message)
            messages.success(request, 'Ako je e-adresa ispravna, primili ste e-poštu sa uputama o'
                                      ' aktiviranju korisničkog računa.')
            return redirect('register')
        return render(request, self.template_name, {'form': form})


class ActivateAccount(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
            user = None

        if user:
            if user.is_active:
                messages.warning(request, 'Vaš korisnički račun je već aktiviran.'
                                          ' Potrebna je prijava upisom korisničkog imena i lozinke.')
                logout(request)
                return redirect('login')
            else:
                is_token_valid = account_activation_token.check_token(user, token)
                if is_token_valid:
                    user.is_active = True
                    user.save()
                    login(self.request, user)
                    messages.success(self.request, f'{user.username}, Vaš korisnički račun je uspješno aktiviran.')
                    return redirect('home')
                else:
                    messages.error(self.request, f'{user.username}, poveznica za aktivaciju više nije vrijedeća.'
                                                 f' Molimo ponovite postupak izrade računa.')
                    user.delete()
                    return redirect('register')
        else:
            messages.error(request, 'Aktivacija korisničkog računa nije uspješno izvršena.'
                                    ' Pokušajte ponoviti postupak izrade računa.')
            logout(request)
            return redirect('register')
