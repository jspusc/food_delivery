from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms

from users.models import User


class BaseForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].error_messages['required'] = 'Obavezno popuniti.'


class UserLoginForm(BaseForm, AuthenticationForm):
    error_messages = {
        'invalid_login': "Netočni korisnički podaci.",
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Korisničko ime'
        self.fields['password'].widget.attrs['placeholder'] = 'Lozinka'

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )

        return self.cleaned_data


class UserRegisterForm(BaseForm, UserCreationForm):

    error_messages = {
        'password_mismatch': 'Lozinke nisu jednake.'
    }

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        error_messages = {
            'username': {
                'unique': 'Korisničko ime već postoji.',
                'invalid': 'Neispravno ime.'
            },
            'email': {
                'unique': 'Korisnik sa ovom e-adresom već postoji.',
                'invalid': 'Neispravna e-adresa.'
            }
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Korisničko ime'
        self.fields['username'].help_text = 'Korisničko ime može sadržavati najviše 150 znakova. Dozvoljeni su ' \
                                            'brojevi, slova i znakovi @, ., +, - i _.'
        self.fields['email'].widget.attrs['placeholder'] = 'E-adresa'
        self.fields['password1'].widget.attrs['placeholder'] = 'Lozinka'
        self.fields['password1'].help_text = 'Lozinka mora sadržavati najmanje 8 znakova i ne može sadržavati samo ' \
                                             'brojeve. Ne može biti slična informacijama u drugim poljima. '

        self.fields['password2'].widget.attrs['placeholder'] = 'Ponovljena lozinka'
