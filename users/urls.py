from django.urls import path

from django.contrib.auth.views import LogoutView
from users.views.user import UserLoginView, UserRegisterView, ActivateAccount


urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', UserRegisterView.as_view(), name='register'),
    # activate account via email invitation
    path('activate/<uidb64>/<token>/', ActivateAccount.as_view(), name='activate'),
]
