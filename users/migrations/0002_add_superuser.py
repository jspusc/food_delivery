from django.db import migrations

from users.models import User


def add_superuser(apps, schema_editor):
    User.objects.create_superuser(
        username='josip',
        email='josip@email.com',
        password='Pingvini1!!'
    ).save()


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_superuser)
    ]
