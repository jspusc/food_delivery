## FOOD DELIVERY PROJECT

### SETUP LOCAL DEVELOPMENT ENVIRONMENT

##### SETUP DOCKER AND DOCKER COMPOSE
* install docker
    * ```curl -fsSL https://get.docker.com -o get-docker.sh``` download the docker install script
    * ```sudo sh get-docker.sh``` run the script
    * ```sudo usermod -aG docker ${USER}``` - add current user to docker group to use docker commands without sudo
    * ```su ${USER}``` to reevaluate docker group membership (or ```exit``` and ssh back)
    * ```docker run hello-world``` for testing. Output should be: Hello from Docker!... or ```docker -v```
* install docker-compose
    * ```sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose```
    * ```sudo chmod +x /usr/local/bin/docker-compose```
    * ```docker-compose -v```

##### START LOCAL ENVIRONMENT
* after project cloning cd into project root (where Makefile is) and ```make start```
* if you need to rebuild images ```make start-build```
* to stop the environment ```make stop``` which will destroy all running containers
* ```make docker-list``` to get containers statuses as well as list of images, volumes and networks
* other usefull command are inside Makefile

##### SETUP PROJECT INTERPRETER IN PYCHARM PRO
* go to: File | Settings | Project: food_delivery | Python Interpreter | Add
* in "Add python interpreter" sidebar choose "Docker Compose"
    * In "Configuration file(s)" file picker find food_delivery/config/local/docker-compose.yml file
    * In "services" dropdown available services appeared, choose "web" service and submit with "Ok" button
* if all installed modules are listed in "Settings" window click "Ok"
* if no modules listed try to restart Pycharm: File | Invalidate Cache / Restart... and reproduce steps above

##### SETUP DJANGO DEBUGGER
* go to: Run | Edit Configuration
* add (+ sign in left upper corner) python
    * in "Script path" find analytics_platform/manage.py file
    * in "Parameters" type: runserver 0.0.0.0:8000
    * in "Environment variables" type: PYTHONUNBUFFERED=1;DJANGO_SETTINGS_MODULE=analytics_platform.settings
    * choose python interpreter (docker-compose) created in steps above
    * in "Command and options" type: up
    * submit with "Ok"
    
##### HOW TO USE DJANGO TRANSLATIONS MODULE
* marking text (parts of code) as translatable:
    * in .py files:
        * wrap string inside django.utils.translation.gettext method
            * use gettext on strings inside methods and functions whose content will be accessed dynamically
        * wrap string inside django.utils.translation.gettext_lazy method
            * use gettext_lazy evaluation only when evaluating strings once on startup
            * for example in settings.py file or as a class variable
    * in .html files:
        * include {% load i18n %}
        * example: 
            * without translation \<h1>Text to translate\</h1>
            * with translation \<h1>{% trans 'Text to translate' %}\</h1>
    * in .js files:
        * just wrap js strings inside preloaded gettext function
* let Django find all text marked as translatable:
    * ```make messages```
    * now all translatable text should appear in django.po file
        ```python
        #: marked/file/location/file.html:80
        msgid "Text to translate"
        msgstr ""
        ```
      put your translation in ```msgstr ""``` double quotes
* compile translated text
    * ```make compilemessages```
    * django.mo file should now be created (or updated)
* issues:
    * ```make messages``` does not work with .js files
        * when you wrap js text in gettext function, manually edit djangojs.po
        * search for duplicates or ```make compilemessages``` will complain
    * text marked as fuzzy in django.po file
        * that means that there are possible similarities that Django noticed
        * check them and fix them
        * delete fuzzy comment when fixed because fuzzy text is not compilable
* for details check [Django documentation on i18n](https://docs.djangoproject.com/en/2.2/topics/i18n/translation/)

### SETUP DEPLOYMENT AND CI

##### INITIAL SETUP OF UBUNTU SERVER ON DIGITAL OCEAN
* when creating DO droplet on [Digital Ocean](https://www.digitalocean.com) select Ubuntu 18.04 (LTS) x64
* you are asked for ssh key while creating droplet and further instructions assume you provided it
* ```ssh root@<droplet-ip>``` from you local machine
* ```apt-get update -y && apt-get upgrade -y && sudo apt-get install build-essential``` update and install essentials
* add limited user (to deny login as root)
    * ```adduser <username>``` enter a password when prompted and remember it
    * ```add <username> sudo``` add user \<username> to sudo group
    * ```mkdir /home/<username>/.ssh```
    * ```touch /home/<username>/.ssh/authorized_keys```
    * ```cat .ssh/authorized_keys > /home/josip/.ssh/authorized_keys``` copy root user authorized_keys to \<username> user 
    * ```exit```
* ```ssh <username>@<droplet-ip>``` ssh as \<username> user
* ``` sudo chown -R <username>:<username> /home/<username>/``` change ownership in /home/\<username> to user \<username>
* modify ssh configuration (to deny password login and root ssh login)
    * ```sudo vim /etc/ssh/sshd_config```
    * and change to following:
        * PasswordAuthentication no
	    * PermitRootLogin no
    * save and exit
    * ```sudo systemctl restart sshd```

##### SETUP GITLAB RUNNER
* install with apt
    * check [here](https://about.gitlab.com/blog/2016/04/19/how-to-set-up-gitlab-runner-on-digitalocean/) for details
    * install runner on server
        * ```curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash``` to install repository
        * ```sudo apt-get install gitlab-runner``` to install gitlab runner
        * ```gitlab-runner -v``` to verify installation
        * ```usermod -aG docker gitlab-runner && sudo service docker restart``` add gitlab-runner to docker group so you don't get permission denied
* install using binaries (preferred because gitlab-runner user is created)
    * check [here](https://docs.gitlab.com/runner/install/linux-manually.html) for details
    * ```sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"```
    * ```sudo chmod +x /usr/local/bin/gitlab-runner``` give exec permissions
    * ```sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash``` - create GitLab CI user
    * ```sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner``` install
    * ```sudo gitlab-runner start``` run as a service
    * ```usermod -aG docker gitlab-runner && sudo service docker restart``` add gitlab-runner to docker group so you don't get permission denied
    * ssh as limited user created in ubuntu setup and ```sudo su - gitlab-runner``` (enter limited user pass) to change to gitlab-runner user 
* register runner (connect with gitlab project)
    * go to specific project on gitlab.com -> Settings -> CI/CD -> expand Runners
        * disable shared runners
        * on Specific runners section copy URL and token
    * go back to server
    * ```sudo gitlab-runner register```
        * paste URL and token
        * choose shell executor
        * if you have specific tags name them
* some tips
    * list registered runners
        * ```gitlab-runner list```
    * delete registered runners
        *  ```sudo vim /etc/gitlab-runner/config.toml```
        * delete entire section [[runners]] for the runner that you want to remove
    * get help
        * ```gitlab-runner --help```
    * if you need write permissions from your server to gitlab (e.g. automate tag pushing)
        * check [here](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/) and [here](https://nicolaw.uk/GitLabCiPushingWrites) for details
        * if you chose gitlab-runner to run as shell executor
            * create ssh key pair on server as user gitlab-runner in /home/gitlab-runner/.ssh/id_rsa
            * paste public key to GitLab UI in Settings | General | Deploy Key and select write permissions
            * setup remote.user.email and remote.origin.url (explained in /update-version.sh)
    * if you need to enable merge_request pipelines 
        
##### USEFUL TIPS
* [Why is one node swarm better then docker-compose in production](https://github.com/BretFisher/ama/issues/8)
* connect to psql terminal with:
    * ```docker container exec -it food_delivery_local_postgres_1 psql -U food_delivery```
    * ```psql -h postgres -p 5432 -U food_delivery```. This option will ask your user password


##### ISSUES
* [docker stack deploy creates dangling images #31252](https://github.com/moby/moby/issues/31252)
    * this project will create untagged postgres container

