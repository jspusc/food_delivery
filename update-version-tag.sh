#!/bin/bash

# further steps require for CI jobs to have write access to repository
# GitLab runners don't (yet) support pushing changes to repo and only option is Deploy Key setup
# Deploy key setup is explained in README.md file

# configure local git user
git config --global user.email $(git --no-pager show -s --format='%ae' HEAD)
# since repo is cloned with HTTPS we need to change remote.origin.url to SSH
git remote set-url --push origin git@gitlab.com:jspusc/food_delivery.git

# extract version from branch name
BRANCH=$(cat source_branch.txt)
BRANCH_AND_VERSION=(${BRANCH//-/ })
VERSION=${BRANCH_AND_VERSION[1]}
TAG=v${VERSION}

# create tag and push it to origin
git tag -a $TAG -m "Version ${VERSION}"
git push origin $TAG
