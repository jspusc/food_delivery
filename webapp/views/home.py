from django.views.generic import TemplateView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class HomeView(TemplateView):
    template_name = 'home.html'


class AboutView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    # handle permission 403 forbidden page
    permission_required = ('auth.add_permission', 'auth.add_group', 'auth.add_session')
    template_name = 'about.html'


class PizzaListView(ListView):
    pass
