from django.urls import path

from webapp.views.home import HomeView, AboutView, PizzaListView


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('pizza-list/', PizzaListView.as_view(), name='pizza_list'),
]
